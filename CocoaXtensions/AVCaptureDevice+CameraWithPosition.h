//
//  AVCaptureDevice+CameraWithPosition.h
//  CocoaXtensions
//
//  Created by JiangZhping on 16/9/3.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

@interface AVCaptureDevice (CameraWithPosition)

+ (AVCaptureDevice *) deviceWithPosition:(AVCaptureDevicePosition)position;

@end
