//
//  NSObject+AccessByName.m
//  CocoaXtensions
//
//  Created by JiangZhping on 2016/10/1.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import "NSObject+NamedAccess.h"

@implementation NSObject (NamedAccess)

+ (NSDictionary<NSString *,id> *)namedObjectDictionary {
    static NSMutableDictionary<NSString *, id> * dictionary;
    if (!dictionary) {
        dictionary = [NSMutableDictionary new];
    }
    return dictionary;
}

+ (id)retrieveObjectWithName:(NSString *)name {
    NSMutableDictionary * dictionary = (NSMutableDictionary *) [NSObject namedObjectDictionary];
    return dictionary[name];
}

+ (NSString *) nameForGivenObject:(id) object {
    NSDictionary<NSString *,id> * dictionary = [NSObject namedObjectDictionary];
    __block NSString * checkResult;
    [dictionary.allKeys enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (dictionary[obj]) {
            checkResult = obj;
        }
    }];
    return checkResult;
}

+ (void)letObject:(id)object accessibleByName:(NSString *)name {
    NSMutableDictionary * dictionary = (NSMutableDictionary *) [NSObject namedObjectDictionary];
    if ([NSObject nameForGivenObject:self]) {
        [dictionary removeObjectForKey:name];
    }
    dictionary[name] = object;
}

- (NSString *)codeInNamedAccess {
    return [NSObject nameForGivenObject:self];
}

- (void)setCodeInNamedAccess:(NSString *)codeInNamedAccess {
    [NSObject letObject:self accessibleByName:codeInNamedAccess];
}

+ (id) retrieveObjectWithName:(NSString *)name blockingUntil:(NSDate *)dateToStopBlocking {
    while (YES) {
        if (![NSObject retrieveObjectWithName:name] && [[NSDate date] compare:dateToStopBlocking] == NSOrderedAscending) {
            usleep(500);
        } else {
            return [NSObject retrieveObjectWithName:name];
        }
    }
}

+ (void) retrieveObjectWithName:(NSString *)name AsyncWithBlock:(void (^) (id retrievedObject))handleBlock {
    NSTimer * retrievingTimer;
    retrievingTimer = [NSTimer timerWithTimeInterval:0.1 target:self selector:@selector(doAsyncHandle:) userInfo:@[name, handleBlock] repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:retrievingTimer forMode:NSRunLoopCommonModes];
}

+ (void) doAsyncHandle:(NSTimer *)timer {
    NSArray * userInfo = (NSArray *)timer.userInfo;
    void (^handleBlock)(id) = userInfo[1];
    id object = [NSObject retrieveObjectWithName:userInfo[0]];
    if (object) {
        [timer invalidate];
        handleBlock(object);
    }
}

@end
