//
//  NSFileManager+TemporaryFile.m
//  AutoStereogramMetal
//
//  Created by 蒋志平 on 16/7/5.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import "NSFileManager+TemporaryFile.h"

@implementation NSFileManager (TemporaryFile)

- (NSString *)documentsDirectory {
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return paths[0];
}

-(NSString *) createTemporaryDirectory {
    // Create a unique directory in the system temporary directory
    NSString *guid = [NSProcessInfo processInfo].globallyUniqueString;
    NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:guid];
    if (![self createDirectoryAtPath:path withIntermediateDirectories:NO attributes:nil error:nil]) {
        return nil;
    }
    return path;
}

-(NSString *) createTemporaryFileWithExtension:(NSString *)extension {
    NSString * tempPath = [[NSFileManager defaultManager] createTemporaryDirectory];
    NSString *guid = [NSProcessInfo processInfo].globallyUniqueString;
    NSString * tempFileName = [guid stringByAppendingPathExtension:extension];
    return [tempPath stringByAppendingPathComponent:tempFileName];
}


@end
