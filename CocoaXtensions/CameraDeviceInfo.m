//
//  CameraDeviceInfo.m
//  CocoaXtensions
//
//  Created by JiangZhping on 2016/11/16.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import "CameraDeviceInfo.h"

@interface CameraDeviceInfo ()

- (instancetype) initWithResolution:(CGSize)resolution FocalLengthRatio:(CGFloat)ratio;
- (instancetype) initWithResolution:(CGSize)resolution FocalLengthRatio:(CGFloat)ratio FocalCenterDeviation:(CGPoint)deviation;
+ (instancetype) cameraInfoWithResolution:(CGSize)resolution FocalLengthRatio:(CGFloat)ratio;
+ (instancetype) cameraInfoWithResolution:(CGSize)resolution FocalLengthRatio:(CGFloat)ratio FocalCenterDeviation:(CGPoint)deviation;

@end

@implementation CameraDeviceInfo

@dynamic cameraMatrix;
@dynamic xFov;
@dynamic yFov;
@dynamic xSymmetricFov;
@dynamic ySymmetricFov;
@dynamic symmetricFrameSize;
@synthesize croppingExtentInSymmetricFrame = _croppingExtentInSymmetricFrame;

- (instancetype) initWithResolution:(CGSize)resolution FocalLengthRatio:(CGFloat)ratio {
    return [self initWithResolution:resolution FocalLengthRatio:ratio FocalCenterDeviation:CGPointZero];
}

- (instancetype) initWithResolution:(CGSize)resolution FocalLengthRatio:(CGFloat)ratio FocalCenterDeviation:(CGPoint)deviation {
    self = [super init];
    if (self) {
        _maxResolutionLandscape = resolution;
        _currentFrameSize = _maxResolutionLandscape;
        _focalCenterPixel = CGPointMake(resolution.width/2.0, resolution.height/2.0);
        _focalCenterDeviation = deviation;
        _focalCenterPixel = CGPointAddCGPoint(_focalCenterPixel, _focalCenterDeviation);
        _focalLengthRatio = ratio;
        _deviceOrientation = UIDeviceOrientationPortrait;
    }
    return self;
}

+ (instancetype) cameraInfoWithResolution:(CGSize)resolution FocalLengthRatio:(CGFloat)ratio {
    return [CameraDeviceInfo cameraInfoWithResolution:resolution FocalLengthRatio:ratio FocalCenterDeviation:CGPointZero];
}

+ (instancetype)cameraInfoWithResolution:(CGSize)resolution FocalLengthRatio:(CGFloat)ratio FocalCenterDeviation:(CGPoint)deviation {
    return [[CameraDeviceInfo alloc] initWithResolution:resolution FocalLengthRatio:ratio FocalCenterDeviation:deviation];
}

+ (CameraDeviceInfo *) cameraProfileByDeviceCode:(NSString *)deviceCode {
    static NSDictionary<NSString *, CameraDeviceInfo *> * cameraProfileByCode = nil;
    
    static CameraDeviceInfo * iSight_1280_720;
    static CameraDeviceInfo * iSight_1280_960_iPadMini79;
    static CameraDeviceInfo * iSight_1280_960_iPadPro129;
    
    if (!cameraProfileByCode) {
        iSight_1280_720 = [CameraDeviceInfo cameraInfoWithResolution:CGSizeMake(1280, 720) FocalLengthRatio:1037.f/1280.f];
        iSight_1280_960_iPadMini79 = [CameraDeviceInfo cameraInfoWithResolution:CGSizeMake(1280, 960) FocalLengthRatio:1179/1280.f];
        iSight_1280_960_iPadPro129 = [CameraDeviceInfo cameraInfoWithResolution:CGSizeMake(1280, 960) FocalLengthRatio:1179/1280.f FocalCenterDeviation:CGPointMake(150, 0)];
        
        cameraProfileByCode = @{
                                
                                /// TODO check correctness
                                @"iPhone8,2" :iSight_1280_720,
                                @"iPad5,1" :iSight_1280_960_iPadMini79,
                                @"iPad5,2" :iSight_1280_960_iPadMini79,
                                @"iPad6,7" : iSight_1280_960_iPadPro129,
                                @"iPad6,8" : iSight_1280_960_iPadPro129,
                                @"MacBookPro11,5" :iSight_1280_720,
                                @"MacBookPro11,4" :iSight_1280_720,
                                @"MacBookPro11,3" :iSight_1280_720,
                                @"MacBookPro11,2" :iSight_1280_720,
                                @"MacBookPro11,1" :iSight_1280_720,
                                };
    }
    
    return cameraProfileByCode[deviceCode];
}

- (GLKMatrix3) generateCameraMatrixWithFrameSize:(CGSize)frameSize {
    CGPoint frameCenter = CGPointMake(frameSize.width/2.0, frameSize.height/2.0);
    CGPoint deviationInCurrentSize = CGPointMultiplyScalar(frameSize.width > frameSize.height ? self.focalCenterDeviation: CGPointMake(_focalCenterDeviation.y, _focalCenterDeviation.x), MAX(frameSize.width, frameSize.height) / MAX(self.maxResolutionLandscape.width,self.maxResolutionLandscape.height));
    CGPoint focalPoint = frameCenter;
    
    switch (_deviceOrientation) {
        case UIDeviceOrientationPortrait:
        case UIDeviceOrientationLandscapeLeft:
            focalPoint = CGPointAddCGPoint(frameCenter, deviationInCurrentSize);
            _croppingExtentInSymmetricFrame = CGRectMakeWithCGPointAndCGSize(CGPointZero, frameSize);
            break;
        case UIDeviceOrientationLandscapeRight:
        case UIDeviceOrientationPortraitUpsideDown:
            focalPoint = CGPointAddCGPoint(frameCenter, CGPointMake(-deviationInCurrentSize.x, -deviationInCurrentSize.y));
            _croppingExtentInSymmetricFrame = CGRectMakeWithCGPointAndCGSize(CGPointMultiplyScalar(deviationInCurrentSize, 2.0), frameSize);
            break;
        default:
            focalPoint = frameCenter;
            _croppingExtentInSymmetricFrame = CGRectMakeWithCGPointAndCGSize(CGPointZero, frameSize);
    }
    
    if (_deviceOrientation == UIDeviceOrientationPortrait || _deviceOrientation == UIDeviceOrientationPortraitUpsideDown) {
        focalPoint.y = frameSize.height - focalPoint.y;
    }

    CGFloat focalLength = (frameSize.width > frameSize.height ? frameSize.width : frameSize.height) * self.focalLengthRatio;
    GLKMatrix3 cameraMatrix = GLKMatrix3Make(focalLength, 0, 0, 0, focalLength, 0, focalPoint.x, focalPoint.y, 1);
    
    return cameraMatrix;
}


- (GLKMatrix3) cameraMatrix {
    return [self generateCameraMatrixWithFrameSize:self.currentFrameSize];
}

- (CGFloat)xFov {
    GLKMatrix3 cameraMatrix = self.cameraMatrix;
    return GLKMathRadiansToDegrees(atan2(cameraMatrix.m20, cameraMatrix.m00) + atan2(self.currentFrameSize.width - cameraMatrix.m20, cameraMatrix.m00));
}

- (CGFloat)yFov {
    GLKMatrix3 cameraMatrix = self.cameraMatrix;
    return GLKMathRadiansToDegrees(atan2(cameraMatrix.m21, cameraMatrix.m11) + atan2(self.currentFrameSize.height - cameraMatrix.m21, cameraMatrix.m11));
}

- (CGSize)symmetricFrameSize {
    GLKMatrix3 cameraMatrix = self.cameraMatrix;
    CGFloat maxWidth = 2.0 * MAX(cameraMatrix.m20, self.currentFrameSize.width - cameraMatrix.m20);
    CGFloat maxHeight = 2.0 * MAX(cameraMatrix.m21, self.currentFrameSize.height - cameraMatrix.m21);
    return CGSizeMake(maxWidth, maxHeight);
}

- (CGFloat)xSymmetricFov {
    GLKMatrix3 cameraMatrix = self.cameraMatrix;
    CGSize symmetricFrameSize = self.symmetricFrameSize;
    return GLKMathRadiansToDegrees(2.0 * atan2(symmetricFrameSize.width/2.0, cameraMatrix.m00));
}

- (CGFloat)ySymmetricFov {
    GLKMatrix3 cameraMatrix = self.cameraMatrix;
    CGSize symmetricFrameSize = self.symmetricFrameSize;
    return GLKMathRadiansToDegrees(2.0 * atan2(symmetricFrameSize.height/2.0, cameraMatrix.m11));
}



@end
