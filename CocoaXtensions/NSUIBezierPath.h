//
//  NSUIBezierPath.h
//  CameraPipeline
//
//  Created by JiangZhiping on 15/11/3.
//  Copyright © 2015年 JiangZhiping. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>
#import <AVFoundation/AVFoundation.h>
#if !TARGET_OS_IPHONE
#import <AppKit/AppKit.h>
@interface NSUIBezierPath : NSBezierPath
#else
#import <UIKit/UIKit.h>
@interface NSUIBezierPath : UIBezierPath
#endif

#if !TARGET_OS_IPHONE
- (CGPathRef) CGPath CF_RETURNS_NOT_RETAINED;
#endif

@end
