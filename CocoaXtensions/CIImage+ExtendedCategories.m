//
//  CIImage+ExtendedCategories.m
//  CocoaXtensions
//
//  Created by JiangZhping on 16/9/19.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import "CIImage+ExtendedCategories.h"

@implementation CIImage (ExtendedCategories)

@dynamic aspectRatio;

- (float)aspectRatio {
    return self.extent.size.width / self.extent.size.height;
}

- (CIImage *) resizeTo:(CGSize)size {
    CGSize rectifiedSize = CGSizeMake(round(size.width), round(size.height));
    CIImage * resized =  [self imageByApplyingTransform:CGAffineTransformMakeScale(rectifiedSize.width/self.extent.size.width, rectifiedSize.height/self.extent.size.height)];
    if (resized.extent.size.width > rectifiedSize.width || resized.extent.size.height > rectifiedSize.height) {
        resized = [resized imageByCroppingToRect:CGRectMakeWithCGPointAndCGSize(CGPointZero, rectifiedSize)];
    }
    return resized;
}

- (nonnull CIImage *) resizeScaleKeepingAspectRatio:(CGFloat) ratio {
    return [self resizeTo:CGSizeMakeWithGLKVector2(GLKVector2MultiplyScalar(GLKVector2MakeWithCGSize(self.extent.size), ratio))];
}

- (nonnull CIImage *) resizeToFillSizeKeepingAspectRatio:(CGSize)size {
    CGFloat rectRatio = size.width / size.height;
    CGFloat imageRatio = self.extent.size.width / self.extent.size.height;
    
    CGFloat newWidth, newHeight;
    if (rectRatio >= imageRatio) {
        newWidth = imageRatio * size.height;
        newHeight = size.height;
    } else {
        newWidth = size.width;
        newHeight = size.width / imageRatio;
    }
    
    return [self resizeTo:CGSizeMake(newWidth, newHeight)];
}

- (CIImage *) rotateImage:(CGFloat)radian {
    static CIFilter * straightenFilter;
    if (!straightenFilter) {
        straightenFilter = [CIFilter filterWithName:@"CIStraightenFilter"];
    }
    [straightenFilter setValue:self forKey:kCIInputImageKey];
    [straightenFilter setValue:@(radian) forKey:kCIInputAngleKey];
    return [straightenFilter valueForKey:kCIOutputImageKey];
}

- (CIImage *) flipHorizontal {
    CIFilter* transform = [CIFilter filterWithName:@"CIAffineTransform"];
#if TARGET_OS_IPHONE
    NSValue *af = [NSValue valueWithCGAffineTransform:CGAffineTransformMakeScale(-1, 1)];
#else
    NSAffineTransform* af = [NSAffineTransform transform];
    [af scaleXBy:-1 yBy:1];
#endif
    [transform setValue:af forKey:@"inputTransform"];
    [transform setValue:self forKey:kCIInputImageKey];
    return [transform valueForKey:kCIOutputImageKey];
}

- (CIImage *) flipVertical {
    return [self imageByApplyingOrientation:4];
}

- (CIImage *) perspectiveCorrection:(NSArray<CIVector *> *) CIVectorArrayTlTrBlBr {
    CIFilter * perspectiveCorrectionFilter = [CIFilter filterWithName:@"CIPerspectiveCorrection"];
    [perspectiveCorrectionFilter setValue:self forKey:kCIInputImageKey];
    [perspectiveCorrectionFilter setValue:CIVectorArrayTlTrBlBr[0] forKey:@"inputTopLeft"];
    [perspectiveCorrectionFilter setValue:CIVectorArrayTlTrBlBr[1] forKey:@"inputTopRight"];
    [perspectiveCorrectionFilter setValue:CIVectorArrayTlTrBlBr[2] forKey:@"inputBottomLeft"];
    [perspectiveCorrectionFilter setValue:CIVectorArrayTlTrBlBr[3] forKey:@"inputBottomRight"];
    return [perspectiveCorrectionFilter valueForKey:kCIOutputImageKey];
}

- (CIImage *) blend:(CIImage *) anotherImage WithFilter:(NSString *)blendFilterName {
    CIFilter * blendFilter = [CIFilter filterWithName:blendFilterName];
    [blendFilter setValue:anotherImage forKey:kCIInputImageKey];
    [blendFilter setValue:self forKey:kCIInputBackgroundImageKey];
    return [blendFilter valueForKey:kCIOutputImageKey];
}

@end
