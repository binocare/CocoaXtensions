//
//  NSUIViewController.h
//  CocoaXtensions
//
//  Created by JiangZhiping on 16/5/6.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import <Foundation/Foundation.h>
#if !TARGET_OS_IPHONE
#import <AppKit/AppKit.h>
@interface NSUIViewController : NSViewController


#else
#import <UIKit/UIKit.h>
@interface NSUIViewController : UIViewController
#endif

@end
