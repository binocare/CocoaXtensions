//
//  NSUIScreen+ScreenCameraFrameConversion.m
//  CocoaXtensions
//
//  Created by JiangZhping on 16/8/14.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import "NSUIScreen+ScreenCameraFrameConversion.h"

#pragma mark - ScreenCameraFrameConversion category
#if TARGET_OS_IPHONE
@implementation UIScreen (ScreenCameraFrameConversion)
#else
@implementation NSScreen (ScreenCameraFrameConversion)
#endif

#define INCH_TO_MM 25.4f
#define MM_TO_INCH (1.0/25.4)

#pragma mark - dynamic
@dynamic screenPhysicalResolution;
@dynamic screenPhysicalPPI;
@dynamic frontCameraPixelInPhysicalResolution;
@dynamic transformationFromScreenToCamera;
@dynamic currentDisplayResolution;
@dynamic currentDisplayPPI;
@dynamic currentDisplayRatio;
@dynamic frontCameraPixelInCurrentResolution;
#if TARGET_OS_IPHONE
@dynamic backingScaleFactor;
#endif

# pragma mark - declared dynamics

#if TARGET_OS_IPHONE
- (CGFloat) backingScaleFactor {
    return self.scale;
}
#endif

- (CGSize) screenPhysicalResolution {
    AppleDeviceInfo *info = [AppleDeviceInfo currentDeviceProfile];
    if (info) {
        return info.screenPhysicalResolution;
    } else {
        return CGSizeZero;
    }
}

- (float) screenPhysicalPPI {
    AppleDeviceInfo *info = [AppleDeviceInfo currentDeviceProfile];
    if (info) {
        return info.screenPhysicalPPI;
    } else {
        return 0.f;
    }

}

- (CGPoint) frontCameraPixelInPhysicalResolution {
    AppleDeviceInfo *info = [AppleDeviceInfo currentDeviceProfile];
    if (info) {
        return info.equavalentPixelPointForFrontCamera;
    } else {
        return CGPointMake(0.f, 0.f);
    }
}

- (GLKMatrix3) transformationFromScreenToCamera {
#if TARGET_OS_IPHONE
    return GLKMatrix3Make(-1, 0, 0, 0, 1, 0, 0, 0, -1);
#else
    return GLKMatrix3Make(-1, 0, 0, 0, -1, 0, 0, 0, 1);
#endif
}

- (CGSize) currentDisplayResolution {
#if TARGET_OS_IPHONE
    return [NSUIScreen mainScreen].bounds.size;
#else
    return [NSUIScreen mainScreen].frame.size;
#endif
}

- (float) currentDisplayPPI {
    return self.screenPhysicalPPI * self.currentDisplayRatio;
}

- (float) currentDisplayRatio {
    CGFloat currentDisplayMaxLength = self.currentDisplayResolution.width > self.currentDisplayResolution.height ? self.currentDisplayResolution.width : self.currentDisplayResolution.height;
    CGFloat physicalDisplayMaxLength = self.screenPhysicalResolution.width > self.screenPhysicalResolution.height ? self.screenPhysicalResolution.width : self.screenPhysicalResolution.height;
    return 1.f * currentDisplayMaxLength / physicalDisplayMaxLength;
}

- (CGPoint) frontCameraPixelInCurrentResolution {
    return CGPointMultiplyScalar(self.frontCameraPixelInPhysicalResolution, self.currentDisplayRatio);
}

-(GLKMatrix4)transformFromFrontCameraFrameToSceneModelCenteredOnScreenCenter {
    GLKVector2 screenCenterPixel = GLKVector2MultiplyScalar(GLKVector2MakeWithCGSize(self.currentDisplayResolution), 0.5);
    GLKVector3 screenCenterInCameraFrame = [self frontCameraPointFromPixel:screenCenterPixel];
    return GLKMatrix4Multiply(GLKMatrix4TranslateWithVector3(GLKMatrix4Identity, screenCenterInCameraFrame), GLKMatrix4MakeZRotation(-M_PI));
    
}

#pragma mark - declared methods

- (double) screenRotationForOrientation:(UIDeviceOrientation)orientation {
    double radianValue = 0;
    switch (orientation) {
        case UIDeviceOrientationPortrait:
            radianValue = 0;
            break;
        case UIDeviceOrientationLandscapeLeft:
            radianValue = M_PI/2.0;
            break;
        case UIDeviceOrientationLandscapeRight:
            radianValue = -M_PI/2.0;
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            radianValue = M_PI;
            break;
        default:
            radianValue = 0;
            break;
    }
#if TARGET_OS_IPHONE
    return  - radianValue;
#else
    return radianValue;
#endif
}

- (GLKVector2) screenCoordinateCenterInUIDeviceOrientation:(UIDeviceOrientation)orientation {
    GLKVector2 resultVector;
    switch (orientation) {
        case UIDeviceOrientationPortrait:
            resultVector = GLKVector2Make(0, 0);
            break;
        case UIDeviceOrientationLandscapeLeft:
            resultVector = GLKVector2Make(self.screenPhysicalResolution.width,0);
            break;
        case UIDeviceOrientationLandscapeRight:
#if TARGET_OS_IPHONE
            resultVector = GLKVector2Make(0, self.screenPhysicalResolution.height);
#else
            resultVector = GLKVector2Make(self.screenPhysicalResolution.width,0);
#endif
            break;
        case UIDeviceOrientationPortraitUpsideDown:
#if TARGET_OS_IPHONE
            resultVector = GLKVector2MakeWithCGSize(self.screenPhysicalResolution);
#else
            resultVector = GLKVector2MakeWithCGSize(self.screenPhysicalResolution);
#endif
            break;
        default:
            resultVector = GLKVector2Make(0, 0);
    }
    
    return GLKVector2MultiplyScalar(resultVector, self.currentDisplayRatio);
}

- (GLKVector2) transformPixel:(GLKVector2)pointA fromOrientationA:(UIDeviceOrientation)deviceOrientationA intoB:(UIDeviceOrientation)deviceOrientationB {
    GLKVector2 zeroPointForA = [self screenCoordinateCenterInUIDeviceOrientation:deviceOrientationA];
    GLKVector2 zerosPointForB = [self screenCoordinateCenterInUIDeviceOrientation:deviceOrientationB];
    
    double screenAngleForA = [self screenRotationForOrientation:deviceOrientationA];
    double screenAngleForB = [self screenRotationForOrientation:deviceOrientationB];
    // first transform to standard portaint.
    
    GLKVector2 pointInPortaint = GLKVector2Add(GLKMatrix3MultiplyVector2WithTranslation(GLKMatrix3MakeZRotation(-screenAngleForA),pointA), zeroPointForA);
    pointInPortaint = GLKVector2Subtract(pointInPortaint, zerosPointForB);
    GLKVector2 pointInB = GLKMatrix3MultiplyVector2WithTranslation(GLKMatrix3MakeZRotation(screenAngleForB),pointInPortaint);
    return pointInB;
}

- (GLKVector3) frontCameraPointFromPixel:(GLKVector2)pixel WithDeviceOrientation:(UIDeviceOrientation)deviceOrientation {
    // first obtain the inver rotation angle.
    double inverseRotationRadian =  - [self screenRotationForOrientation:deviceOrientation];
    // obtain the Zero (in the given device orientation)'s position in standard portait coordinate system.
    GLKVector2 coordinateZeroInPortraitScreen = [self screenCoordinateCenterInUIDeviceOrientation:deviceOrientation];
    // then obtain the given pixel's cooridnate in portait view.
    GLKVector2 pixelCoordinateInPortraitScreen = GLKVector2Add(GLKMatrix3MultiplyVector2WithTranslation(GLKMatrix3MakeZRotation(inverseRotationRadian),pixel), coordinateZeroInPortraitScreen);
    // the pixel distance to the camera
    pixelCoordinateInPortraitScreen = GLKVector2Subtract(pixelCoordinateInPortraitScreen, GLKVector2MakeWithCGPoint(self.frontCameraPixelInCurrentResolution));
    // rotate the pixel_to_camera into portait view
    GLKVector2 pixelInPortraitCameraFrame = GLKMatrix3MultiplyVector2WithTranslation(self.transformationFromScreenToCamera,pixelCoordinateInPortraitScreen);
    // then rotate to the current camera orientation,
    // note that the rotation angle is inversed.
    GLKVector2 pixelInCameraFrame = GLKMatrix3MultiplyVector2WithTranslation(GLKMatrix3MakeZRotation( inverseRotationRadian),pixelInPortraitCameraFrame);
    // transformed into milimeter.
    pixelInCameraFrame = GLKVector2DivideScalar(pixelInCameraFrame, self.currentDisplayPPI);
    pixelInCameraFrame = GLKVector2MultiplyScalar(pixelInCameraFrame, INCH_TO_MM);
    return GLKVector3MakeWithVector2(pixelInCameraFrame, 0.f);
}

- (GLKVector3) frontCameraPointFromPixel:(GLKVector2)pixel {
#if TARGET_OS_IPHONE
    UIDeviceOrientation currentOrientation = [[UIDevice currentDevice] orientation];
    if (currentOrientation == UIDeviceOrientationUnknown) {
        CGSize currentResolution = self.currentDisplayResolution;
        if (currentResolution.width >= currentResolution.height) {
            currentOrientation = UIDeviceOrientationLandscapeLeft;
        } else {
            currentOrientation = UIDeviceOrientationPortrait;
        }
    }
#else
    UIDeviceOrientation currentOrientation = UIDeviceOrientationPortrait;
#endif
    return [self frontCameraPointFromPixel:pixel WithDeviceOrientation:currentOrientation];
}

- (GLKVector2) pixelFromFrontCameraPoint:(GLKVector3) point WithDeviceOrientation:(UIDeviceOrientation)deviceOrientation {
    GLKVector2 frontCameraPixel2D = GLKVector3GetGLKVector2(point);
    frontCameraPixel2D = GLKVector2MultiplyScalar(frontCameraPixel2D, MM_TO_INCH * self.currentDisplayPPI);
    frontCameraPixel2D = GLKMatrix3MultiplyVector2WithTranslation(GLKMatrix3Transpose(self.transformationFromScreenToCamera), frontCameraPixel2D);
    double inverseRotationRadian =  - [self screenRotationForOrientation:deviceOrientation];
    // convert to portrait camera view
    GLKVector2 pixelInPortraitFrontCamera = GLKMatrix3MultiplyVector2WithTranslation(GLKMatrix3MakeZRotation(inverseRotationRadian), frontCameraPixel2D);
    GLKVector2 pointInPortraitScreen = GLKVector2Add(GLKVector2MakeWithCGPoint(self.frontCameraPixelInCurrentResolution), pixelInPortraitFrontCamera);
    // get screen pixel in portrait view
    GLKVector2 pointInTargetOrientation = [self transformPixel:pointInPortraitScreen fromOrientationA:UIDeviceOrientationPortrait intoB:deviceOrientation];

    return pointInTargetOrientation;
}

- (GLKVector2) pixelFromFrontCameraPoint:(GLKVector3) point {
#if TARGET_OS_IPHONE
    UIDeviceOrientation currentOrientation = [[UIDevice currentDevice] orientation];
#else
    UIDeviceOrientation currentOrientation = UIDeviceOrientationPortrait;
#endif
    return [self pixelFromFrontCameraPoint:point WithDeviceOrientation:currentOrientation];
}

- (GLKVector3) sceneModelScreenCenterFramePointFromFrontCamearPoint:(GLKVector3) point {
    return GLKMatrix4MultiplyVector3WithTranslation(self.transformFromFrontCameraFrameToSceneModelCenteredOnScreenCenter, point);
}

@end
