//
//  NSUIColor+ColorComponents.m
//  CocoaXtensions
//
//  Created by JiangZhping on 2016/10/27.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import "NSUIColor+ColorComponents.h"

#if !TARGET_OS_IPHONE
@implementation NSColor (ColorComponents)
#else
@implementation UIColor (ColorComponents)

@dynamic redComponent;
@dynamic greenComponent;
@dynamic blueComponent;
@dynamic alphaComponent;

- (CGFloat)redComponent {
    CGFloat r, g, b, a;
    [self getRed:&r green:&g blue:&b alpha:&a];
    return r;
}

- (CGFloat)greenComponent {
    CGFloat r, g, b, a;
    [self getRed:&r green:&g blue:&b alpha:&a];
    return g;
}

- (CGFloat)blueComponent {
    CGFloat r, g, b, a;
    [self getRed:&r green:&g blue:&b alpha:&a];
    return b;
}

- (CGFloat)alphaComponent {
    CGFloat r, g, b, a;
    [self getRed:&r green:&g blue:&b alpha:&a];
    return a;
}

#endif

@end

