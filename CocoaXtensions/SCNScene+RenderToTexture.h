//
//  SCNScene+RenderToTexture.h
//  CocoaXtensions
//
//  Created by JiangZhping on 16/8/28.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import <SceneKit/SceneKit.h>
#import <Metal/Metal.h>
#import "FoundationConversions.h"
#import "NSUIColor.h"
#import "NSUIColor+ColorComponents.h"
#import "CIVector+GLKitConversion.h"
#import "CIImage+CocoaFormatsConversion.h"
#import "CIImage+ExtendedCategories.h"
#import "CGExtensions.h"
#import "SCNNode+Shortcuts.h"
#import "SCNRenderer+FastFarPlaneProjection.h"

@interface SCNScene (RenderToTexture)

- (SCNRenderer *) cachedSCNRendererForPointOfView:(SCNNode *) nodeWithCamera;

- (SCNVector3) fastProjectPointToFarClippingPlane:(SCNVector3)point fromPointOfView:(SCNNode *) nodeWithCamera;

- (SCNVector3) fastProjectPointToFarClippingPlane:(SCNVector3)point fromPointOfView:(SCNNode *) nodeWithCamera canvasSize:(CGSize)canvasSize;

- (id) renderToTextureFromPointOfView:(SCNNode *) nodeWithCamera withPerspectiveCorrectionForWorldQuadrilateral:(NSArray<CIVector *> *)quadrilateralPoints fullCanvasSize:(CGSize) fullCanvasSize canvasBackgroundColor:(NSUIColor *)canvasColor;

- (id) renderToTextureFromPointOfView:(SCNNode *) nodeWithCamera canvasSize:(CGSize)canvasSize canvasBackgroundColor:(NSUIColor *)color;

@end
