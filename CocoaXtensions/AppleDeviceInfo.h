//
//  AppleDeviceInfo.h
//  CocoaXtensions
//
//  Created by JiangZhping on 2016/9/23.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HeaderBase.h"
#import "NSUIScreen.h"
#import <sys/utsname.h>
#include <sys/sysctl.h>
#import "CameraDeviceInfo.h"



@interface AppleDeviceInfo : NSObject

@property (nonatomic) NSString * deviceCode;
@property (nonatomic) NSString * productName;
@property (nonatomic) CGSize screenPhysicalResolution;
@property (nonatomic) CGFloat screenPhysicalPPI;
@property (nonatomic) CGPoint equavalentPixelPointForFrontCamera;
@property (nonatomic) CameraDeviceInfo * frontCamera;
@property (nonatomic) CameraDeviceInfo * rearCamera;

+ (instancetype) currentDeviceProfile;
+ (instancetype) deviceInfoWithResolution:(CGSize)resolution PPI:(float)screenPPI EquavalentPoint:(CGPoint)point;

@end
