//
//  CocoaXtensions.h
//  CocoaXtensions
//
//  Created by JiangZhping on 16/8/12.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import <CocoaXtensions/HeaderBase.h>

// Shimming codes
#import <CocoaXtensions/NSUIView.h>
#import <CocoaXtensions/NSUIColor.h>
#import <CocoaXtensions/NSUIBezierPath.h>
#import <CocoaXtensions/NSUIImage.h>
#import <CocoaXtensions/NSUIViewController.h>
#import <CocoaXtensions/NSUIScreen.h>
#import <CocoaXtensions/NSValue+CommonNamings.h>


// Cocoa catgegories
#import <CocoaXtensions/FoundationConversions.h>
#import <CocoaXtensions/NSObject+NamedAccess.h>
#import <CocoaXtensions/NSNumber+Random.h>
#import <CocoaXtensions/GLKitExtensions.h>
#import <CocoaXtensions/CGExtensions.h>
#import <CocoaXtensions/NSFileManager+TemporaryFile.h>
#import <CocoaXtensions/NSOperationQueue+SharedQueue.h>
#import <CocoaXtensions/NSUserDefaults+CheckExistsBeforeSetting.h>
#import <CocoaXtensions/NSUIColor+RandomColor.h>
#import <CocoaXtensions/NSUIColor+ColorComponents.h>
#import <CocoaXtensions/NSUIView+Shortcuts.h>
#import <CocoaXtensions/NSUIScreen+ScreenCameraFrameConversion.h>
#import <CocoaXtensions/NSTimer+TimerWithBlock.h>
#import <CocoaXtensions/NSString+ExtendedUserDefaults.h>

// AFFoundation enhancement
#import <CocoaXtensions/AVCaptureDevice+CameraWithPosition.h>

// SceneKit enhancement
#import <CocoaXtensions/SCNNode+Shortcuts.h>
#import <CocoaXtensions/SCNScene+RenderToTexture.h>
#import <CocoaXtensions/SCNRenderer+FastFarPlaneProjection.h>
#import <CocoaXtensions/SCNHitTestResult+GLKitConversion.h>
#import <CocoaXtensions/SCNView+GLKProjection.h>

// CIImage based image formats conversion
#import <CocoaXtensions/CIVector+GLKitConversion.h>
#import <CocoaXtensions/CIImage+CocoaFormatsConversion.h>
#import <CocoaXtensions/CIImage+ExtendedCategories.h>

// harmless utilities
#import <CocoaXtensions/AppleDeviceInfo.h>
#import <CocoaXtensions/CameraDeviceInfo.h>



