//
//  NSUIView+Shortcuts.h
//  CocoaXtensions
//
//  Created by JiangZhping on 2016/10/1.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSUIView.h"
#if !TARGET_OS_IPHONE
#import <AppKit/AppKit.h>
@interface NSView (Shortcuts)


#else
#import <UIKit/UIKit.h>
@interface UIView (Shortcuts)
#endif

@property (nonatomic) float viewAspectRatio;

- (void) addConstraintFillParentWidth;
- (void) addConstraintFillParentHeight;

@end
