//
//  NSUIColor.h
//  CameraPipeline
//
//  Created by JiangZhiping on 15/11/3.
//  Copyright © 2015年 JiangZhiping. All rights reserved.
//

#import <Foundation/Foundation.h>
#if !TARGET_OS_IPHONE
#import <AppKit/AppKit.h>
@interface NSUIColor : NSColor
#else
#import <UIKit/UIKit.h>
@interface NSUIColor : UIColor
#endif

@end
