//
//  NSFileManager+TemporaryFile.h
//  AutoStereogramMetal
//
//  Created by 蒋志平 on 16/7/5.
//  Copyright © 2016年 JiangZhiping. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSFileManager (TemporaryFile)


/**
 Shortcut to the Documents directory

 @return The path to Documents directory
 */
-(NSString *) documentsDirectory;
-(NSString *) createTemporaryDirectory;
-(NSString *) createTemporaryFileWithExtension:(NSString *)extension;

@end
