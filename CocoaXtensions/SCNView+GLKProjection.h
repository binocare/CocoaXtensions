//
//  SCNView+GLKProjection.h
//  CocoaXtensions
//
//  Created by JiangZhping on 16/9/10.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import <SceneKit/SceneKit.h>
#import "GLKitExtensions.h"

@interface SCNView (GLKProjection)

- (GLKVector3) glkProjectPoint:(GLKVector3)point;
- (GLKVector3) glkUnprojectPoint:(GLKVector3)point;

@end
