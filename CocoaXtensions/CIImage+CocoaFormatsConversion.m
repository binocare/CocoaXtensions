//
//  CIImage+CocoaFormatsConversion.m
//  CocoaXtensions
//
//  Created by JiangZhping on 16/8/29.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import "CIImage+CocoaFormatsConversion.h"

@implementation CIImage (CocoaFormatsConversion)

@dynamic cgImage;
@dynamic nsuiImage;
@dynamic cvPixelBuffer;
@dynamic mtlTexture;

+ (nullable instancetype) imageWithContent:(nonnull id)content {
    
    if (!content) {
        return nil;
    }
    
    if ([content isKindOfClass:[CIImage class]]) {
        return (CIImage *)content;
    }
    
    if ([content conformsToProtocol:@protocol(MTLTexture)]) {
        return [CIImage imageWithMTLTexture:content options:nil];
    }
    
    if ([content isKindOfClass:[NSUIImage class]]) {
        NSUIImage * nsuiImage = (NSUIImage *) content;
#if TARGET_OS_IPHONE
        return [CIImage imageWithContent:(__bridge id)nsuiImage.CGImage];
#else
        NSData * tiffData = [nsuiImage TIFFRepresentationUsingCompression:NSTIFFCompressionNone factor:0.f];
        return [CIImage imageWithContent:tiffData];
#endif
    }
    
    if (CFGetTypeID((__bridge CFTypeRef)(content)) == CGImageGetTypeID()) {
        return [CIImage imageWithCGImage:(__bridge CGImageRef) content];
    }
    
    if (CFGetTypeID((__bridge CFTypeRef)(content)) == CVPixelBufferGetTypeID()) {
        return [CIImage imageWithCVPixelBuffer:(__bridge CVPixelBufferRef) content];
    }
    
    if ([content isKindOfClass:[NSString class]]) {
        return [CIImage imageWithContentsOfURL:[NSURL fileURLWithPath:content]];
    }
    
    if ([content isKindOfClass:[NSURL class]]) {
        return [CIImage imageWithContentsOfURL:content];
    }
    
    if ([content isKindOfClass:[NSData class]]) {
        return [CIImage imageWithData:content];
    }
    
    return nil;
}

- (CGImageRef) cgImage {
    static CIContext * context;
    static CGImageRef image = NULL;
    static CGColorSpaceRef colorSpace = NULL;
    
    if(!colorSpace) {
        colorSpace = CGColorSpaceCreateDeviceRGB();
    }
    
    
    if ((image = self.CGImage)) {
        return image;
    }
    
    if (self.pixelBuffer) {
        CVPixelBufferRef pixelBuffer = self.pixelBuffer;
        size_t width = CVPixelBufferGetWidthOfPlane(pixelBuffer, 0);
        size_t height = CVPixelBufferGetHeightOfPlane(pixelBuffer, 0);
        size_t bytesPerRow = CVPixelBufferGetBytesPerRowOfPlane(pixelBuffer, 0);
        CVPixelBufferGetPixelFormatType(pixelBuffer);
        CVPixelBufferLockBaseAddress(pixelBuffer, 0);
        uint8_t *baseAddress = (uint8_t *)CVPixelBufferGetBaseAddress(pixelBuffer);
        CGContextRef context = CGBitmapContextCreate(baseAddress, width, height, 8, bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
        CGImageRef imageRef = CGBitmapContextCreateImage(context);
        
        CVPixelBufferUnlockBaseAddress(pixelBuffer,0);
        CGContextRelease(context);
        
        return imageRef;
    }
    
    if (!context) {
        id<MTLDevice> device = MTLCreateSystemDefaultDevice();
        @synchronized (context) {
            context = [CIContext contextWithMTLDevice:device];
        }
    }
    image = [context createCGImage:self fromRect:self.extent];
    return image;
}

-(NSUIImage *)nsuiImage {
#if TARGET_OS_IPHONE
    return (NSUIImage *)[NSUIImage imageWithCIImage:self];
#else
        NSCIImageRep * rep = [NSCIImageRep imageRepWithCIImage:self];
        NSImage * nsImage = [[NSImage alloc] initWithSize:rep.size];
        [nsImage addRepresentation:rep];
        return (NSUIImage *)nsImage;
#endif
}

- (CVPixelBufferRef) cvPixelBuffer {
    static CIContext * context;
    static CVPixelBufferRef pb;
    
    if ((pb = self.pixelBuffer)) {
        return pb;
    }
    
    if (self.CGImage) {
        CGImageRef image = self.CGImage;
        CVPixelBufferRef pxbuffer = NULL;
        NSDictionary *options = @{(id)kCVPixelBufferCGImageCompatibilityKey: @YES,
                                 (id)kCVPixelBufferCGBitmapContextCompatibilityKey: @YES};
        
        size_t width =  CGImageGetWidth(image);
        size_t height = CGImageGetHeight(image);
        size_t bytesPerRow = CGImageGetBytesPerRow(image);
        
        CFDataRef  dataFromImageDataProvider = CGDataProviderCopyData(CGImageGetDataProvider(image));
        uint8_t  *imageData = (uint8_t *)CFDataGetBytePtr(dataFromImageDataProvider);
        
        CVPixelBufferCreateWithBytes(kCFAllocatorDefault,width,height,kCVPixelFormatType_32BGRA,imageData,bytesPerRow,NULL,NULL,(__bridge CFDictionaryRef)options,&pxbuffer);
        
        CFRelease(dataFromImageDataProvider);
        
        return pxbuffer;
    }
    
    if (!context) {
        id<MTLDevice> device = MTLCreateSystemDefaultDevice();
        context = [CIContext contextWithMTLDevice:device];
    }
    
    CVReturn status = CVPixelBufferCreate(kCFAllocatorDefault, self.extent.size.width, self.extent.size.height, kCVPixelFormatType_32BGRA, (__bridge CFDictionaryRef)@{(__bridge NSString *)kCVPixelBufferCGImageCompatibilityKey:@YES, (__bridge NSString *)kCVPixelBufferMetalCompatibilityKey:@YES, (__bridge NSString *)kCVPixelBufferCGBitmapContextCompatibilityKey:@YES}, &pb);
    
    if (status == kCVReturnSuccess) {
        [context render:self toCVPixelBuffer:pb];
    }
    return pb;
}

- (id<MTLTexture>) mtlTexture {
    if (self.pixelBuffer) {
        CVPixelBufferRef pixelBuffer = self.pixelBuffer;
        CVReturn error;
        size_t width = CVPixelBufferGetWidth(pixelBuffer);
        size_t height = CVPixelBufferGetHeight(pixelBuffer);
        
        CVMetalTextureRef textureRef;
        CVMetalTextureCacheRef _videoTextureCache = NULL;
        error = CVMetalTextureCacheCreateTextureFromImage(kCFAllocatorDefault, _videoTextureCache, pixelBuffer, NULL, MTLPixelFormatBGRA8Unorm, width, height, 0, &textureRef);
        
        if (error)
        {
            NSLog(@">> ERROR: Couldnt create texture from image");
            assert(0);
        }
        
        id<MTLTexture> resultTexture = CVMetalTextureGetTexture(textureRef);
        if (!resultTexture) {
            NSLog(@">> ERROR: Couldn't get texture from texture ref");
            assert(0);
        }
        CVBufferRelease(textureRef);
        return resultTexture;

    }
    return [self mtlTextureWithCommandBuffer:nil];
}

- (id<MTLTexture>) mtlTextureWithCommandBuffer:(id<MTLCommandBuffer>)buffer {
    id<MTLTexture> texture = nil;
    texture = [self renderToMTLTexture:texture commandBuffer:buffer];
    return texture;
}

- (id<MTLTexture>) renderToMTLTexture:(id<MTLTexture>) targetTexture commandBuffer:(id<MTLCommandBuffer>)buffer {
    static id<MTLDevice> device;
    static CIContext * context;
    if (!device) {
        device = MTLCreateSystemDefaultDevice();
    }
    if (!context) {
        context = [CIContext contextWithMTLDevice:device];
    }
    if (!targetTexture) {
        MTLTextureDescriptor * descriptor = [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:MTLPixelFormatRGBA8Unorm width:self.extent.size.width height:self.extent.size.height mipmapped:NO];
        descriptor.usage = MTLTextureUsageShaderRead | MTLTextureUsageShaderWrite;
        targetTexture = [device newTextureWithDescriptor:descriptor];
    }
        [context render:self toMTLTexture:targetTexture commandBuffer:buffer bounds:self.extent colorSpace:CGColorSpaceCreateDeviceRGB()];
        if (!buffer) {
            [buffer commit];
            [buffer waitUntilCompleted];
        }
    
    return targetTexture;
}

- (void) saveToFile:(NSString *) filePathWithExtension {
    CGImageRef cgImage = self.cgImage;
    NSData * imageData = nil;
    if ([filePathWithExtension.pathExtension.lowercaseString isEqualToString:@"png"]) {
#if TARGET_OS_IPHONE
        UIImage *uiImage = [UIImage imageWithCGImage:cgImage];
        imageData = UIImagePNGRepresentation(uiImage);
#else
        NSBitmapImageRep * rep = [[NSBitmapImageRep alloc] initWithCGImage:cgImage];
        imageData = [rep representationUsingType:NSBitmapImageFileTypePNG properties:@{}];
#endif
    }
    
    if ([filePathWithExtension.pathExtension.lowercaseString isEqualToString:@"jpg"]) {
#if TARGET_OS_IPHONE
        UIImage *uiImage = [UIImage imageWithCGImage:cgImage];
        imageData = UIImageJPEGRepresentation(uiImage,0.95f);
#else
        NSBitmapImageRep * rep = [[NSBitmapImageRep alloc] initWithCGImage:cgImage];
        imageData = [rep representationUsingType:NSBitmapImageFileTypeJPEG properties:@{}];
#endif
    }
    
    [imageData writeToFile:filePathWithExtension atomically:NO];
    CGImageRelease(cgImage);
}

# pragma mark - conversion shortcuts

+ (CGImageRef)conversionCGImageFromCVPixelBuffer:(CVPixelBufferRef)pixelBuffer {
    return [CIImage imageWithContent:(__bridge id)pixelBuffer].cgImage;
}

+ (CGImageRef)conversionCGImageFromFromNSUIImage:(NSUIImage *)nsuiImage {
    return [CIImage imageWithContent:nsuiImage].cgImage;
}

+ (CGImageRef)conversionCGImageFromFromMTLTexture:(id<MTLTexture>)mtlTexture {
    return [CIImage imageWithContent:mtlTexture].cgImage;
}

+ (CVPixelBufferRef)conversionCVPixelBufferFromCGImage:(CGImageRef)cgImage {
    return [CIImage imageWithContent:(__bridge id)cgImage].cvPixelBuffer;
}

+ (CVPixelBufferRef)conversionCVPixelBufferFromNSUIImage:(NSUIImage *)nsuiImage {
    return [CIImage imageWithContent:nsuiImage].cvPixelBuffer;

}

+ (CVPixelBufferRef)conversionCVPixelBufferFromMTLTexture:(id<MTLTexture>)mtlTexture {
    return [CIImage imageWithContent:mtlTexture].cvPixelBuffer;

}

+ (NSUIImage *)conversionNSUIImageFromCGImage:(CGImageRef)cgImage {
    return [CIImage imageWithContent:(__bridge id)cgImage].nsuiImage;
}

+ (NSUIImage *)conversionNSUIImageFromCVPixelBuffer:(CVPixelBufferRef)pixelBuffer {
    return [CIImage imageWithContent:(__bridge id)pixelBuffer].nsuiImage;
}

+ (NSUIImage *)conversionNSUIImageFromMTLTexture:(id<MTLTexture>)mtlTexture {
    return [CIImage imageWithContent:mtlTexture].nsuiImage;
}

+ (id<MTLTexture>)conversionMTLTextureFromCGImage:(CGImageRef)cgImage {
    return [CIImage imageWithContent:(__bridge id)cgImage].mtlTexture;
}

+ (id<MTLTexture>)conversionMTLTextureFromCVPixelBuffer:(CVPixelBufferRef)pixelBuffer {
    return [CIImage imageWithContent:(__bridge id)pixelBuffer].mtlTexture;
}

+ (id<MTLTexture>)conversionMTLTextureFromNSUIImage:(NSUIImage *)nsuiImage {
    return [CIImage imageWithContent:nsuiImage].mtlTexture;
}

@end
