//
//  NSValue+CommonNamings.h
//  CocoaXtensions
//
//  Created by JiangZhping on 16/9/3.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#if TARGET_OS_IPHONE
#import <UIKit/UIKit.h>
#else
#import <AppKit/AppKit.h>
#endif

@interface NSValue (CommonNamings)

#if TARGET_OS_IPHONE

+ (instancetype) valueWithPoint:(CGPoint)point;
+ (instancetype) valueWithSize:(CGSize)size;
+ (instancetype) valueWithRect:(CGRect)rect;

- (CGPoint) pointValue;
- (CGRect) rectValue;
- (CGSize) sizeValue;

#else 

+ (instancetype) valueWithCGPoint:(CGPoint)point;
+ (instancetype) valueWithCGSize:(CGSize)size;
+ (instancetype) valueWithCGRect:(CGRect)rect;

- (CGPoint) CGPointValue;
- (CGRect) CGRectValue;
- (CGSize) CGSizeValue;
#endif

@end
