//
//  SCNRenderer+FastFarPlaneProjection.h
//  CocoaXtensions
//
//  Created by JiangZhping on 16/9/2.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import <SceneKit/SceneKit.h>
#import "CGExtensions.h"

@interface SCNRenderer (FastFarPlaneProjection)

- (SCNVector3) fastProjectPointToFarClippingPlane:(SCNVector3)point;

- (SCNVector3) fastProjectPointToFarClippingPlane:(SCNVector3)point canvasSize:(CGSize)canvasSize;

@end
