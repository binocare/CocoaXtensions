//
//  NSUserDefaults+CheckExistsBeforeSetting.m
//  AutostereogramApp
//
//  Created by JiangZhping on 16/8/12.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import "NSUserDefaults+CheckExistsBeforeSetting.h"

@implementation NSUserDefaults (CheckExistsBeforeSetting)

- (BOOL) keyExists:(nonnull NSString *) defaultName {
    return [self objectForKey:defaultName] != nil;
}

- (void) setBool:(BOOL)value ifKeyNotExists:(NSString *)defaultName {
    if (![self keyExists:defaultName]) {
        [self setBool:value forKey:defaultName];
        [self synchronize];
    }
}

- (void) setDouble:(float)value ifKeyNotExists:(NSString *)defaultName {
    if (![self keyExists:defaultName]) {
        [self setDouble:value forKey:defaultName];
        [self synchronize];
    }
}

- (void) setFloat:(float)value ifKeyNotExists:(NSString *)defaultName {
    if (![self keyExists:defaultName]) {
        [self setFloat:value forKey:defaultName];
        [self synchronize];
    }
}

- (void) setObject:(id)value ifKeyNotExists:(NSString *)defaultName {
    if (![self keyExists:defaultName]) {
        [self setObject:value forKey:defaultName];
        [self synchronize];
    }
}

- (void) setInteger:(NSInteger)value ifKeyNotExists:(NSString *)defaultName {
    if (![self keyExists:defaultName]) {
        [self setInteger:value forKey:defaultName];
        [self synchronize];
    }
}

- (void) setURL:(NSURL *)value ifKeyNotExists:(NSString *)defaultName {
    if (![self keyExists:defaultName]) {
        [self setURL:value forKey:defaultName];
        [self synchronize];
    }
}

- (BOOL) boolForKey:(NSString *)defaultName ifKeyNotExistsSetDefault:(BOOL) value {
    [self setBool:value ifKeyNotExists:defaultName];
    return [self boolForKey:defaultName];
}

- (double) doubleForKey:(NSString *)defaultName ifKeyNotExistsSetDefault:(double) value {
    [self setDouble:value ifKeyNotExists:defaultName];
    return [self doubleForKey:defaultName];
}

- (float) floatForKey:(NSString *)defaultName ifKeyNotExistsSetDefault:(float) value {
    [self setFloat:value ifKeyNotExists:defaultName];
    return [self floatForKey:defaultName];
}

- (NSInteger) integerForKey:(NSString *)defaultName ifKeyNotExistsSetDefault:(NSInteger) value {
    [self setInteger:value ifKeyNotExists:defaultName];
    return [self integerForKey:defaultName];
}

- (id) objectForKey:(NSString *)defaultName ifKeyNotExistsSetDefault:(id) value {
    [self setObject:value ifKeyNotExists:defaultName];
    return [self objectForKey:defaultName];
}

- (NSString *) stringForKey:(NSString *)defaultName ifKeyNotExistsSetDefault:(NSString *) value {
    [self setObject:value ifKeyNotExists:defaultName];
    return [self stringForKey:defaultName];
}

- (NSURL *) URLForKey:(NSString *)defaultName ifKeyNotExistsSetDefault:(NSURL *) value {
    [self setURL:value ifKeyNotExists:defaultName];
    return [self URLForKey:defaultName];
}

@end
