//
//  NSNumber+Random.h
//  CocoaXtensions
//
//  Created by JiangZhping on 16/9/9.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <GameplayKit/GameplayKit.h>

@interface NSNumber (Random)

+(CGFloat) randomFloat;
+(CGFloat) randomFloatSmallerThan:(CGFloat)maxValue;
+(CGFloat) randomFloatBetween:(CGFloat)minValue and:(CGFloat)maxValue;

+ (NSInteger) randomInteger;
+ (NSInteger) randomIntegerSmallerThan:(NSInteger)maxValue;
+ (NSInteger) randomIntegerBetween:(NSInteger)minValue and:(NSInteger)maxValue;

+ (BOOL) randomBOOL;

@end
