//
//  CIVector+GLKitConversion.m
//  CocoaXtensions
//
//  Created by JiangZhping on 16/8/30.
//  Copyright © 2016年 JiangZhping. All rights reserved.
//

#import "CIVector+GLKitConversion.h"

@implementation CIVector (GLKitConversion)

+ (instancetype) vectorWithGLKVector2:(GLKVector2) glkv2 {
    return [CIVector vectorWithX:glkv2.x Y:glkv2.y];
}

+ (instancetype) vectorWithGLKVector3:(GLKVector3) glkv3 {
    return [CIVector vectorWithX:glkv3.x Y:glkv3.y Z:glkv3.z];
}

+ (instancetype) vectorWithGLKVector4:(GLKVector4) glkv4 {
    return [CIVector vectorWithX:glkv4.x Y:glkv4.y Z:glkv4.z W:glkv4.w];
}

+ (instancetype) vectorWithSCNVector3:(SCNVector3) scnv3 {
    return [CIVector vectorWithX:scnv3.x Y:scnv3.y Z:scnv3.z];
}

+ (instancetype) vectorWithSCNVector4:(SCNVector4) scnv4 {
    return [CIVector vectorWithX:scnv4.x Y:scnv4.y Z:scnv4.z W:scnv4.w];
}

- (GLKVector2)GLKVector2Value {
    return GLKVector2Make(self.X, self.Y);
}

- (GLKVector3)GLKVector3Value {
    return GLKVector3Make(self.X, self.Y, self.Z);
}

- (GLKVector4)GLKVector4Value {
    return GLKVector4Make(self.X, self.Y, self.Z, self.W);
}

- (SCNVector3)SCNVector3Value {
    return SCNVector3Make(self.X, self.Y, self.Z);
}

- (SCNVector4)SCNVector4Value {
    return SCNVector4Make(self.X, self.Y, self.Z, self.W);
}

@end
